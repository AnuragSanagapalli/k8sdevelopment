package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
public class BlueApplication {

	private Logger logger=LoggerFactory.getLogger(BlueApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(BlueApplication.class, args);
	}

	@Value("${msg:default}")
    private String msg;
	
	@RequestMapping("/response")
	public String blue(String MSG)
	{
		logger.info("Hello Sleuth");
		return this.msg=MSG;
	}
	
	

}

